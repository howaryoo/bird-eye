NodeRED webhook Relay 

Step by step instructions

1: build the docker images

    docker-compose build

2: run

    docker-compose run
    
    
3: configuration

Point your browser to URL: http://localhost:1880/
Configure the nodes with the corresponding credentials:

 * Webhook Relay Node: Key + Secret
 * MongoDB Atlas: Username + Password
 
4: Deploy the flow

 Click on the red `Deploy` button on the top right corner of the NodeRed UI.
 Webhook notifications should come in and are saved in MongoDB.
 
 
 